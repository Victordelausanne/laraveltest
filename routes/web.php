<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Input;
use App\Models\Film;
use App\User;


Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::resource ('artiste', 'ArtisteController');
Route::resource ('film', 'FilmController');
Route::resource ('cinema', 'CinemaController');
Route::resource ('seance', 'SeanceController');
Route::resource ('salle', 'SalleController');

// Route::any('/search',function(){
//     $q = Input::get ( 'q' );
//     $film = Film::where('titre','LIKE','%'.$q.'%')->orWhere('annee','LIKE','%'.$q.'%')->get();
//     if(count($film) > 0)
//         return view('welcome')->withDetails($film)->withQuery ( $q );
//         else return view ('welcome')->withMessage('Aucune séance trouvée');
// });

Auth::routes();

Route::get('index','SearchController@search');


Route::get('/home', 'HomeController@index')->name('home');
