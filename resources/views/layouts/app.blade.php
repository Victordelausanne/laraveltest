<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="{{asset('css/app.css')}}?v={{filemtime(public_path('css/app.css'))}}" rel="stylesheet" type="text/css">
  </head>
  <body>
    <p>
    </p>
    <div class="container">
      @yield('content')
    </div>
    @if (session('ok'))
    <div class="container">
      <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{session('ok')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
          <span aria-hidden="true"> &time; </span>
        </button>
      </div>
    </div>
    @endif
    <script src="{{ asset('js/app.js') }}?v={{ filemtime(public_path('js/app.js')) }}">
    </script>
  </body>
</html>
