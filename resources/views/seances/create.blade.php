@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('seance.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <p class="bg-info">
      <label for="titre_film">Titre du film</label>
      <input type="text" name="titre_film" value="">
      @if ($errors->has('titre_film'))
        <div class="invalid-feedback">
          {{$errors->first('titre_film')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="nom_cinema">Nom du cinema</label>
      <input type="text" name="nom_cinema" value="">
      @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
          {{$errors->first('nom_cinema')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="no_salle">Numéro de salle</label>
      <input type="text" name="no_salle" value="">
      @if ($errors->has('no_salle'))
        <div class="invalid-feedback">
          {{$errors->first('no_salle')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="heure_debut">Heure Début</label>
      <input type="number" name="heure_debut" value="">
      @if ($errors->has('heure_debut'))
        <div class="invalid-feedback">
          {{$errors->first('heure_debut')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="heure_fin">Heure fin</label>
      <input type="number" name="heure_fin" value="">
      @if ($errors->has('heure_fin'))
        <div class="invalid-feedback">
          {{$errors->first('heure_fin')}}
        </div>
      @endif
    </p>
    <button type="submit">Créer</button>
  </form>
@endsection
