@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('seance.update', $seance->id)}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <p class="bg-info">
      <label for="titre_film">Titre du film</label>
      <input type="text" name="titre_film" value="{{$seance->titre_film}}">
      @if ($errors->has('titre_film'))
        <div class="invalid-feedback">
          {{$errors->first('titre_film')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="nom_cinema">Nom du cinema</label>
      <input type="text" name="nom_cinema" value="{{$seance->nom_cinema}}">
      @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
          {{$errors->first('nom_cinema')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="no_salle">Numéro de salle</label>
      <input type="text" name="no_salle" value="{{$seance->no_salle}}">
      @if ($errors->has('no_salle'))
        <div class="invalid-feedback">
          {{$errors->first('no_salle')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="heure_debut">Heure Début</label>
      <input type="number" name="heure_debut" value="{{$seance->heure_debut}}">
      @if ($errors->has('heure_debut'))
        <div class="invalid-feedback">
          {{$errors->first('heure_debut')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="heure_fin">Heure fin</label>
      <input type="number" name="heure_fin" value="{{$seance->heure_fin}}">
      @if ($errors->has('heure_fin'))
        <div class="invalid-feedback">
          {{$errors->first('heure_fin')}}
        </div>
      @endif
    </p>
    <button type="submit">Mettre à jour</button>
  </form>
@endsection
