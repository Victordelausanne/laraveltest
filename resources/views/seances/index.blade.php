@extends('layouts.app')
@section('title', 'page Title')
@section('content')
<table class="table table-striped table-centered">
  <thead>
    <tr>
      <th>{{ __('Titre du film') }}</th>
      <th>{{ __('Nom du cinema') }}</th>
      <th>{{ __('Numéro de salle') }}</th>
      <th>{{ __('Heure de début') }}</th>
      <th>{{ __('Heure de fin') }}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($seances as $seance)
    <tr>
      <td> {{$seance->titre_film}}</td>
      <td> {{$seance->nom_cinema}}</td>
      <td> {{$seance->no_salle}}</td>
      <td> {{$seance->heure_debut}}</td>
      <td> {{$seance->heure_fin}}</td>
      <td class="table-action">
        <a type="button" href="{{ route('seance.edit',
        $seance->id)}}" style="background-color:blue" class="btn btn-sm" data-toggle="tooltip"
        title="@lang('Modifier lseance') {{$seance->id}}">
          <i class="fas fa-edit fa-lg"> </i>
          Editer
        </a>
        <a type="button" href="{{ route('seance.destroy',
        $seance->id)}}" id="delete" style="background-color:red" class="btn btn-sm danger" data-toggle="tooltip"
        title="@lang('supprimer lseance') {{$seance->id}}">
          <i class="fas fa-edit fa-lg"> </i>
          Supprimer
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$seances->appends(request()->except('page'))->links()}}

@endsection
