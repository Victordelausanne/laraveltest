@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('film.store')}}" method="post">
    {{csrf_field()}}
    <p class="bg-info">
      <label for="titre">titre</label>
      <input type="text" name="titre" value="">
      @if ($errors->has('titre'))
        <div class="invalid-feedback">
          {{$errors->first('titre')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="artiste_id">Réalisateur</label>
      <input type="text" name="artiste_id" value="">
      @if ($errors->has('artiste_id'))
        <div class="invalid-feedback">
          {{$errors->first('artiste_id')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="annee">Année de création</label>
      <input type="number" name="annee" value="">
      @if ($errors->has('annee'))
        <div class="invalid-feedback">
          {{$errors->first('annee')}}
        </div>
      @endif
    </p>
    <button type="submit">Créer</button>
  </form>

  {{$errors}}
@endsection
