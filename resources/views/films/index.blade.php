@extends('layouts.app')
@section('title', 'page Title')
@section('content')
<table class="table table-striped table-centered">
  <thead>
    <tr>
      <th>{{ __('titre') }}</th>
      <th>{{ __('Annee') }}</th>
      <th>{{ __('Realisateur') }}</th>

    </tr>
  </thead>
  <tbody>
    @foreach($films as $film)
    <tr>
      <td> {{ $film->titre}}</td>
      <td> {{ $film->annee}}</td>
      <td> {{ $film->artiste_id}}</td>


      <td class="table-action">
        <a type="button" href="{{ route('film.edit',
        $film->id)}}" style="background-color:blue" class="btn btn-sm" data-toggle="tooltip"
        title="@lang('Modifier le film') {{$film->titre}}">
          <i class="fas fa-edit fa-lg"> </i>
          Editer
        </a>
        <a type="button" href="{{ route('film.destroy',
        $film->id)}}" id="delete" style="background-color:red" class="btn btn-sm danger" data-toggle="tooltip"
        title="@lang('supprimer le film') {{$film->titre}}">
          <i class="fas fa-edit fa-lg"> </i>
          Supprimer
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$films->appends(request()->except('page'))->links()}}

@endsection
