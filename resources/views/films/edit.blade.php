@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('film.update', $film->id)}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <p class="bg-info">
      <label for="titre">titre</label>
      <input type="text" name="titre" value="{{$film->titre}}">
      @if ($errors->has('titre'))
        <div class="invalid-feedback">
          {{$errors->first('titre')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="artiste_id">Réalisateur</label>
      <input type="text" name="artiste_id" value="{{$film->artiste_id}}">
      @if ($errors->has('artiste_id'))
        <div class="invalid-feedback">
          {{$errors->first('artiste_id')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="annee">Année de création</label>
      <input type="number" name="annee" value="{{$film->annee}}">
      @if ($errors->has('annee'))
        <div class="invalid-feedback">
          {{$errors->first('annee')}}
        </div>
      @endif
    </p>
    <button type="submit">Mettre à jour</button>
  </form>
@endsection
