@extends('layouts.app')
@section('title', 'page Title')
@section('content')
<table class="table table-striped table-centered">
  <thead>
    <tr>
      <th>{{ __('Nom du cinema') }}</th>
      <th>{{ __('Ville') }}</th>
      <th>{{ __('Adresse') }}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($cinemas as $cinema)
    <tr>
      <td> {{ $cinema->nom_cinema}}</td>
      <td> {{ $cinema->ville}}</td>
      <td> {{ $cinema->adresse}}</td>
      <td class="table-action">
        <a type="button" href="{{ route('cinema.edit',
        $cinema->id)}}" style="background-color:blue" class="btn btn-sm" data-toggle="tooltip"
        title="@lang('Modifier le cinema') {{$cinema->nom_cinema}}">
          <i class="fas fa-edit fa-lg"> </i>
          Editer
        </a>
        <a type="button" href="{{ route('cinema.destroy',
        $cinema->id)}}" id="delete" style="background-color:red" class="btn btn-sm danger" data-toggle="tooltip"
        title="@lang('supprimer le cinema') {{$cinema->nom_cinema}}">
          <i class="fas fa-edit fa-lg"> </i>
          Supprimer
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$cinemas->appends(request()->except('page'))->links()}}

@endsection
