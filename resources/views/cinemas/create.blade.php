@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('cinema.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <p class="bg-info">
      <label for="nom_cinema">Nom du cinema</label>
      <input type="text" name="nom_cinema" value="">
      @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
          {{$errors->first('nom_cinema')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="ville">Ville</label>
      <input type="text" name="ville" value="">
      @if ($errors->has('Ville'))
        <div class="invalid-feedback">
          {{$errors->first('Ville')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="adresse">Adresse</label>
      <input type="text" name="Adresse" value="">
      @if ($errors->has('Adresse'))
        <div class="invalid-feedback">
          {{$errors->first('Adresse')}}
        </div>
      @endif
    </p>
    <button type="submit">Créer</button>
  </form>
@endsection
