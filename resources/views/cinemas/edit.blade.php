@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('cinema.update', $cinema->id)}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <p class="bg-info">
      <label for="nom_cinema">Nom du cinema</label>
      <input type="text" name="nom_cinema" value="{{$cinema->nom_cinema}}">
      @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
          {{$errors->first('nom_cinema')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="ville">Ville</label>
      <input type="text" name="ville" value="{{$cinema->ville}}">
      @if ($errors->has('ville'))
        <div class="invalid-feedback">
          {{$errors->first('ville')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="adresse">Adresse</label>
      <input type="text" name="adresse" value="{{$cinema->adresse}}">
      @if ($errors->has('adresse'))
        <div class="invalid-feedback">
          {{$errors->first('adresse')}}
        </div>
      @endif
    </p>
    <button type="submit"> Mettre à jour </button>
  </form>
@endsection
