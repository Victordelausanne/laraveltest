<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->

        <!-- Styles -->
        <style>
            html, body {
                background-color: #000;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .display-all{
              height: 50px;
              text-align: center;
              color: #fff;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Séance de cinema
                </div>
                <!-- <form action="/search" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input type="text" class="form-control" name="q">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                              Rechercher un film
                            </button>
                        </span>
                    </div>
                </form> -->
                <form method="GET" action="{{url('index')}}">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="search" class="form-control" placeholder="Rechercher">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary">Recherche</button>
                        </div>
                    </div>
                </form>
                <div class="container">
                      @if(isset($films))
                      <table class="table table-striped">
                          <thead>
                              <tr>
                                  <th>Titre</th>
                                  <th>annee</th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach($films as $film)
                              <tr>
                                  <td>{{$film->titre}}</td>
                                  <td>{{$film->annee}}</td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      @endif
                  </div>
                  <a class="flex-center display-all" href="artiste">
                    Voir tous les artistes
                  </a>
                  <a class="flex-center display-all" href="film">
                    Voir tous les films
                  </a>
                  <a class="flex-center display-all" href="seance">
                    Voir toutes les seances
                  </a>
                  <a class="flex-center display-all" href="cinema">
                    Voir tous les cinemas
                  </a>
                  <a class="flex-center display-all" href="salle">
                    Voir tous les salles
                  </a>
            </div>
        </div>
    </body>
</html>
