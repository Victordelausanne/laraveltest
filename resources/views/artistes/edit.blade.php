@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('artiste.update', $artiste->id)}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <p class="bg-info">
      <label for="name">Nom</label>
      <input type="text" name="nom" value="{{$artiste->nom}}">
      @if ($errors->has('name'))
        <div class="invalid-feedback">
          {{$errors->first('name')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="prenom">Prenom</label>
      <input type="text" name="prenom" value="{{$artiste->prenom}}">
      @if ($errors->has('prenom'))
        <div class="invalid-feedback">
          {{$errors->first('prenom')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="annee_naissance">Anne de naissance</label>
      <input type="number" name="annee_naissance" value="{{$artiste->annee_naissance}}">
      @if ($errors->has('annee_naissance'))
        <div class="invalid-feedback">
          {{$errors->first('annee_naissance')}}
        </div>
      @endif
    </p>
    <input type="file" name="poster">
    <button type="submit">Mettre a jour</button>
  </form>
@endsection
