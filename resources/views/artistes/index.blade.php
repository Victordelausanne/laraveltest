@extends('layouts.app')
@section('title', 'page Title')
@section('content')
<table class="table table-striped table-centered">
  <thead>
    <tr>
      <th>{{ __('Nom') }}</th>
      <th>{{ __('Prenom') }}</th>
      <th>{{ __('Annee de naissance') }}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($artistes as $artiste)
    <tr>
      <td> {{ $artiste->nom}}</td>
      <td> {{ $artiste->prenom}}</td>
      <td> {{ $artiste->annee_naissance}}</td>
      <td class="table-action">
        <a type="button" href="{{ route('artiste.edit',
        $artiste->id)}}" style="background-color:blue" class="btn btn-sm" data-toggle="tooltip"
        title="@lang('Modifier lartiste') {{$artiste->nom}}">
          <i class="fas fa-edit fa-lg"> </i>
          Editer
        </a>
        <a type="button" href="{{ route('artiste.destroy',
        $artiste->id)}}" id="delete" style="background-color:red" class="btn btn-sm danger" data-toggle="tooltip"
        title="@lang('supprimer lartiste') {{$artiste->nom}}">
          <i class="fas fa-edit fa-lg"> </i>
          Supprimer
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$artistes->appends(request()->except('page'))->links()}}

@endsection
