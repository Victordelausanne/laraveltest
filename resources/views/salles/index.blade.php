@extends('layouts.app')
@section('title', 'page Title')
@section('content')
<table class="table table-striped table-centered">
  <thead>
    <tr>
      <th>{{ __('Nom du cinema') }}</th>
      <th>{{ __('Numéro de salle') }}</th>
      <th>{{ __('Taille') }}</th>
      <th>{{ __('Climatisation') }}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($salles as $salle)
    <tr>
      <td> {{$salle->nom_cinema}}</td>
      <td> {{$salle->no_salle}}</td>
      <td> {{$salle->taille}}</td>
      <td> {{$salle->climatisation}}</td>
      <td class="table-action">
        <a type="button" href="{{ route('salle.edit',
        $salle->id)}}" class="btn btn-sm" data-toggle="tooltip"
        title="@lang('Modifier la salle') {{$salle->id}}">
          <i class="fas fa-edit fa-lg"> </i>
          Editer
        </a>
        <a type="button" href="{{ route('salle.destroy',
        $salle->id)}}" id="delete" class="btn btn-sm danger" data-toggle="tooltip"
        title="@lang('supprimer la salle') {{$salle->id}}">
          <i class="fas fa-edit fa-lg"> </i>
          Supprimer
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$salles->appends(request()->except('page'))->links()}}

@endsection
