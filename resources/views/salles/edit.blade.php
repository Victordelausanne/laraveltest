@extends('layouts.app')
@section('title', 'page Title')
@section('content')
  <form class="form-horizontal" action="{{ route('salle.update', $salle->id)}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <p class="bg-info">
      <label for="nom_cinema">Nom cinema</label>
      <input type="text" name="nom_cinema" value="{{$salle->nom_cinema}}">
      @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
          {{$errors->first('nom_cinema')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="no_salle">Numéro de Salle</label>
      <input type="number" name="no_salle" value="{{$salle->no_salle}}">
      @if ($errors->has('no_salle'))
        <div class="invalid-feedback">
          {{$errors->first('no_salle')}}
        </div>
      @endif
    </p>
    <p class="bg-info">
      <label for="taille">Taille</label>
      <input type="number" name="Taille" value="{{$salle->taille}}">
      @if ($errors->has('taille'))
        <div class="invalid-feedback">
          {{$errors->first('taille')}}
        </div>
      @endif
    </p>
    <button type="submit">Mettre à jour</button>
  </form>
@endsection
