<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToArtisteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artistes', function (Blueprint $table) {
          $table->unsignedBigInteger('user_id')->default(1)->nullable();
          // $table->string('name', 255);
          // $table->string('email',255);
          // $table->timestamps('email_verified_at',255);
          // $table->string('password',255);
          // $table->string('remember_token',255);
          // $table->timestamps('created_at',255);
          // $table->timestamps('updated_at',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artiste', function (Blueprint $table) {
            //
        });
    }
}
