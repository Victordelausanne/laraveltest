<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('seances', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('titre_film');
          $table->foreign('titre_film')->references('id')->on('films');
          $table->unsignedBigInteger('no_salle');
          $table->foreign('no_salle')->references('id')->on('salles');
          $table->string('heure_debut', 50);
          $table->string('heure_fin', 50);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seances');
    }
}
