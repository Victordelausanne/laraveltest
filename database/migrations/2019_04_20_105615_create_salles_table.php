<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salles', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('cinema');
          $table->foreign('cinema')->references('id')->on('cinemas');
          $table->integer('no_salle');
          $table->integer('taille');
          $table->boolean('climatisation');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salles');
    }
}
