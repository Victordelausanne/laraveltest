<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insert(
        [
          [
          'titre' => 'Jai toujours rêvé dêtre un gangster',
          'annee' => 1938,
          'artiste_id' => Artiste::where('nom', 'Allen')->first()->id,
          ],[
          'titre' => 'Les Cadavres ne portent pas de costard',
          'annee' => 1946
          'artiste_id' => Artiste::where('nom', 'lynch')->first()->id,

          ],[
          'titre' => 'Arrête ou ma mère va tirer',
          'annee' => 1943
          'artiste_id' => Artiste::where('nom', 'Allen')->first()->id,

          ],[
          'titre' => 'Jai rencontré le Diable',
          'annee' => 1959
          'artiste_id' => Artiste::where('nom', 'Kubrick')->first()->id,

          ],[
          'titre' => 'Un après-midi de chien',
          'annee' => 1964
          'artiste_id' => Artiste::where('nom', 'Lucas')->first()->id,

          ]
        ]);
    }
}
