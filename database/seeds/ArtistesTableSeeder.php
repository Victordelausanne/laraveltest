<?php

use Illuminate\Database\Seeder;

class ArtistesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artistes')->insert(
        [
          [
          'nom' => 'Allen',
          'prenom' => 'Woody',
          'annee_naissance' => 1938,
          ],[
          'nom' => 'lynch',
          'prenom' => 'David',
          'annee_naissance' => 1946
          ],[
          'nom' => 'Kubrick',
          'prenom' => 'Stanley',
          'annee_naissance' => 1943
          ],[
          'nom' => 'Lucas',
          'prenom' => 'Georges',
          'annee_naissance' => 1959
          ],[
          'nom' => 'Quentin',
          'prenom' => 'Tarantino',
          'annee_naissance' => 1964
          ],[
          'nom' => 'Spielberg',
          'prenom' => 'Stevens',
          'annee_naissance' => 1953
          ],[
          'nom' => 'Nolan',
          'prenom' => 'Christopher',
          'annee_naissance' => 1956
          ],[
          'nom' => 'Jackson',
          'prenom' => 'Peter',
          'annee_naissance' => 1946
          ],[
          'nom' => 'Gille',
          'prenom' => 'Lelouche',
          'annee_naissance' => 1959
          ],[
          'nom' => 'Cyril',
          'prenom' => 'Hanouna',
          'annee_naissance' => 1959
          ],[
          'nom' => 'Hitchcock',
          'prenom' => 'David',
          'annee_naissance' => 1934
          ],[
          'nom' => 'Tim',
          'prenom' => 'Burton',
          'annee_naissance' => 1958
          ],
        ]);
    }
}
