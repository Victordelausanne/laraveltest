<?php

use Illuminate\Database\Seeder;

class SallesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      [
      'no_salle' => '8',
      'taille' => '123',
      'climatisation' => '1',
      'cinema' => 'Rex'
      ],[
      'no_salle' => '1',
      'taille' => '421',
      'climatisation' => '0',
      'cinema' => 'Rex'
      ],[
      'no_salle' => '3',
      'taille' => '123',
      'climatisation' => '1',
      'cinema' => 'Pathe'
      ],
    }
}
