<?php

use Illuminate\Database\Seeder;

class SeancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    [
      [
      'titre_film' => 'Jai toujours rêvé dêtre un gangster',
      'no_salle' => '8',
      'heure_debut' => '1900',
      'heure_fin' => '2030'
      ],[
      'titre_film' => 'Jai toujours rêvé dêtre un gangster',
      'no_salle' => '3',
      'heure_debut' => '1800',
      'heure_fin' => '1930'
      ],[
      'titre_film' => 'Jai toujours rêvé dêtre un gangster',
      'no_salle' => '1',
      'heure_debut' => '1000',
      'heure_fin' => '1130'
      ]
    ]
    }
}
