<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seance;
use App\Http\Requests\SeanceRequest;
use Image;
use App\Notifications\SeanceCreated;
use App\User;


class SeanceController extends Controller
{

    public function __construct(){
      $this->middleware('ajax')->only('destroy');
      $this->middleware('auth')->only('edit');
      $this->middleware('auth')->only('destroy');
      $this->middleware('auth')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('seances.index', ['seances'=> \App\Models\Seance::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('seances.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeanceRequest $request)
    {
      $seance = Seance::create($request->all());

      Auth()->user()->notify(new SeanceCreated($seance));

      return redirect()->route ('home')
                          ->with('ok', __ ('seance a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seance $seance)
    {
      $this->authorize('seance.update', $seance);
      return view('seances.edit', ['seance' => $seance]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeanceRequest $request, Seance $seance)
    {
      $seance->update( $request->all() );
      return redirect()->route('seance.index')
                       ->with('ok', __('L seance a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Seance $seance)
    {
      $seance->delete();
      return response()->json();
    }


}
