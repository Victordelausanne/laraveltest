<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Http\Requests\FilmRequest;
use App\Notifications\FilmCreated;
use App\User;


class FilmController extends Controller
{

    public function __construct(){
      $this->middleware('ajax')->only('destroy');
      $this->middleware('auth')->only('edit');
      $this->middleware('auth')->only('destroy');
      $this->middleware('auth')->only('create');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('films.index', ['films'=> Film::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('films.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FilmRequest $request)
    {
        Film::create($request->all());
        return redirect()->route ('home')
                          -> with ('ok', __ ('Le film a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Film $film)
    {
      $this->authorize('film.update', $film);
      return view('films.edit', ['film' => $film]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FilmRequest $request, Film $film)
    {
        $film->update( $request->all() );
        Auth()->user()->notify(new FilmCreated($film));
        return redirect()->route('film.index')
                         ->with('ok', __('Lfilm a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Film $film)
    {
      $film->delete();
      return response()->json();
    }


}
