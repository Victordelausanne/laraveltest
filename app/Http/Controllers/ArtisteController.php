<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artiste;
use App\Http\Requests\ArtisteRequest;
use App\Notifications\ArtisteCreated;
use App\User;
use Image;

class ArtisteController extends Controller
{

    public function __construct(){
      $this->middleware('ajax')->only('destroy');
      $this->middleware('auth')->only('edit');
      $this->middleware('auth')->only('destroy');
      $this->middleware('auth')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artistes.index', ['artistes'=> \App\Models\Artiste::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('artistes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtisteRequest $request)
    {
      $artiste = Artiste::create($request->all());
      Auth()->user()->notify(new ArtisteCreated($artiste));
      return redirect()->route('artiste.index')
                          ->with('ok', __ ('artiste a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Artiste $artiste)
    {
      $this->authorize('artiste.update', $artiste);
      return view('artistes.edit', ['artiste' => $artiste]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtisteRequest $request, Artiste $artiste)
    {
      $artiste->update( $request->all() );
      return redirect()->route('artiste.index')
                       ->with('ok', __('L artiste a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Artiste $artiste)
    {
      $artiste->delete();
      return response()->json();
    }


}
