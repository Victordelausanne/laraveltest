<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cinema;
use App\Http\Requests\CinemaRequest;
use App\Notifications\CinemaCreated;
use App\User;


class CinemaController extends Controller
{

    public function __construct(){
      $this->middleware('ajax')->only('destroy');
      $this->middleware('auth')->only('edit');
      $this->middleware('auth')->only('destroy');
      $this->middleware('auth')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cinemas.index', ['cinemas'=> \App\Models\Cinema::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('cinemas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CinemaRequest $request)
    {
      $cinema = Cinema::create($request->all());
      Auth()->user()->notify(new CinemaCreated($cinema));
      return redirect()->route('cinema.index')
                          ->with('ok', __ ('cinema a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cinema $cinema)
    {
      $this->authorize('cinema.update', $cinema);
      return view('cinemas.edit', ['cinema' => $cinema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CinemaRequest $request, Cinema $cinema)
    {
      $cinema->update( $request->all() );
      return redirect()->route('cinema.index')
                       ->with('ok', __('L cinema a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Cinema $cinema)
    {
      $cinema->delete();
      return response()->json();
    }


}
