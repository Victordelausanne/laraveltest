<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Salle;
use App\Http\Requests\SalleRequest;
use App\Notifications\SalleCreated;
use App\User;


class SalleController extends Controller
{

    public function __construct(){
      $this->middleware('ajax')->only('destroy');
      $this->middleware('auth')->only('edit');
      $this->middleware('auth')->only('destroy');
      $this->middleware('auth')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('salles.index', ['salles'=> \App\Models\Salle::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('salles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalleRequest $request)
    {
      $salle = Salle::create($request->all());
      Auth()->user()->notify(new SalleCreated($salle));
      return redirect()->route ('salle.idex')
                          ->with('ok', __ ('salle a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Salle $salle)
    {
      $this->authorize('salle.update', $salle);
      return view('salles.edit', ['salle' => $salle]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalleRequest $request, Salle $salle)
    {
      $salle->update( $request->all() );
      return redirect()->route('salle.index')
                       ->with('ok', __('La salle a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Salle $salle)
    {
      $salle->delete();
      return response()->json();
    }


}
