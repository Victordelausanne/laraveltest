<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Cinema;


class CinemaPolicy
{
    use HandlesAuthorization;

    public function update(User $user, cinema $cinema){
      return $user->id === $cinema->user_id;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
