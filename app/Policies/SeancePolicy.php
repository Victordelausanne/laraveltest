<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Seance;


class SeancePolicy
{
    use HandlesAuthorization;

    public function update(User $user, cinema $seance){
      return $user->id === $seance->user_id;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
