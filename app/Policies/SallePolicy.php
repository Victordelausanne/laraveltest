<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Salle;


class SeancePolicy
{
    use HandlesAuthorization;

    public function update(User $user, salle $salle){
      return $user->id === $salle->user_id;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
