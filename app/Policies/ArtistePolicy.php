<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Artiste;


class ArtistePolicy
{
    use HandlesAuthorization;

    public function update(User $user, artiste $artiste){
      return $user->id === $artiste->user_id;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
