<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Film;


class FilmPolicy
{
    use HandlesAuthorization;

    public function update(User $user, film $film){
      return $user->id === $film->user_id;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
