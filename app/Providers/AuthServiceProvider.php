<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Artiste;
use App\Policies\ArtistePolicy;
use App\Models\Film;
use App\Policies\FilmPolicy;
use App\Models\Salle;
use App\Policies\SallePolicy;
use App\Models\Seance;
use App\Policies\SeancePolicy;
use App\Models\Cinema;
use App\Policies\CinemaPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
         Artiste::class => ArtistePolicy::class,
         Film::class => FilmPolicy::class,
         Salle::class => SallePolicy::class,
         Seance::class => SeancePolicy::class,
         Cinema::class => CinemaPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::resource('artiste', ArtistePolicy::class);
        Gate::resource('film', FilmPolicy::class);
        Gate::resource('Salle', SallePolicy::class);
        Gate::resource('Seance', SeancePolicy::class);
        Gate::resource('Cinema', CinemaPolicy::class);
    }
}
