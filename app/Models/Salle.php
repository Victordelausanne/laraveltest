<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
  protected $fillable = [
    'cinema', 'no_salle', 'taille', 'climatisation'
  ];

  public function cinema_salle()
  {
    return $this->belongsToOne('App\Models\Cinema');
  }

  public function projection_room()
  {
    return $this->belongsToOne('App\Models\Projection');
  }

}
