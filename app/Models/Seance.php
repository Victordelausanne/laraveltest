<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
  protected $fillable = [
    'titre_film', 'no_salle', 'heure_debut', 'heure_fin'
  ];

  public function salle_seance()
  {
    return $this->hasOne('App\Models\Salle');
  }

  public function film_seance()
  {
    return $this->hasOne('App\Models\Film');
  }

}
