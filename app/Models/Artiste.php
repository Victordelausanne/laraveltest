<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artiste extends Model
{
  // use SoftDeletes;
  
    protected $fillable = [
      'nom', 'prenom', 'annee_naissance'
    ];

    public function films_realises(){
      return $this->hasMany('App/models/Film');
    }

    public function films_joues(){
      return $this->belongsToMany('App/models/Film')->withPivot('nom_role');
    }
}
