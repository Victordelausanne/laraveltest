<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
  protected $fillable = [
    'nom_cinema', 'ville', 'adresse'
  ];

  public function salle_cinema()
  {
    return $this->hasMany('App\Models\Salle');
  }

}
