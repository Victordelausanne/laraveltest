<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;


class Film extends Model
{
  use SearchableTrait;

  protected $fillable = [
    'titre', 'annee', 'artiste_id'
  ];

  protected $searchable = [
     'columns' => [
         'films.titre' => 10,
         'film.annee' => 5,
     ]
 ];

  public function realisateur(){
    return $this->belongsTo('App/models/Artiste');
  }

  public function acteurs(){
    return $this->belongsToMany('App/models/Artiste');
  }
}
